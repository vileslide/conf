# Sammel-PDFs mit globaler Nummerierung erstellen

## Inhaltsverzeichnis

1. [PDFs kombinieren](#pdfs-kombinieren)
1. [Globale Nummerierung erstellen](#globale-nummerierung-erstellen)
   1. [Globale Nummerierung bearbeiten](#globale-nummerierung-erstellen)


### Einleitung

Dieses Tutorial zeigt, wie man mehrere einzelne PDFs zu einem gesammelten PDF kombinieren kann und eine globale Nummerierung hinzufügt.

Die verwendete Software ist der Adobe Acrobat X Pro. Für neuere Versionen sollte der Ablauf ident sein. Auch andere PDF-Reader bieten die Funktionalitäten an, beispielsweise der [PDF-XChange Editor](https://www.tracker-software.com/product/pdf-xchange-editor) in der kostenlosen Variante. Zumindest für das Kombinieren der PDFs gibt es auch Open Source Software, z.B. [pdfmerge](https://sourceforge.net/projects/pdfmerge/)



## PDFs kombinieren

Im geöffneten Adobe Acrobat Pro klickt man links oben auf **Datei > Erstellen > Dateien in einem einzigen PDF-Dokument zusammenführen...**

![PDFs zusammenführen Menü](tut/Sammel-PDFs/img/1.png)

Es öffnet sich ein neues Fenster. Hier kann man wahlweise über den Punkt **"Dateien hinzufügen..." (1)** oder **Drag & Drop** die zu kombinierenden PDFs hinzufügen. Wichtig ist, rechts unten bei **Dateigröße** die **größte Einstellung** zu **wählen (2)**. Damit werden die PDFs unverändert kombiniert. Bei den beiden anderen Einstellungen werden im PDF integrierte Rasterbilder in ihrer Auflösung reduziert, um die gesamte Dateigröße zu verringern.

![PDFs hinzufügen und Dateigröße wählen](tut/Sammel-PDFs/img/2.png)

Nachdem die PDFs hinzugefügt wurden, muss man noch die Reihenfolge kontrollieren. Hierbei gilt, dass die Reihenfolge von oben nach unten durchlaufen wird, das oberste PDF liefert dann also die ersten Seiten für das Sammel-PDF. Die **Reihenfolge** kann **per Drag & Drop** geändert werden oder man wählt ein PDF aus und verwendet die **Schaltflächen links unten (1)**. Wenn die Reihenfolge passt, kann man rechts unten auf **"Dateien zusammenführen"** klicken **(2)**

![Reihenfolge der PDFs anpassen](tut/Sammel-PDFs/img/3.png)

Nach der Zusammenführung öffnet sich ein neues Fenster mit dem kombinierten PDF. Dieses sollte man als Erstes über Datei > Speichern unter abspeichern.


## Globale Nummerierung erstellen

Falls man eine Globale Nummerierung hinzufügen möchte, blendet man die **Werkzeuge-Toolbar** ein **(1)**. Anschließend klickt man auf **Kopf- und Fußzeile (2)** und auf **"Kopf- und Fußzeile hinzufügen" (3)**

![Kopf- oder Fußzeile hinzufügen](tut/Sammel-PDFs/img/4.png)

Im neuen Fenster hat man nun einige Möglichkeiten.

1. **Schrift gestalten**
1. **Ränder einstellen** (Nummerierung verschieben)
1. **Bereiche um z.B. die Seitenzahl über (4) hinzuzufügen.** Wenn man die Syntax kennt kann man auch direkt schreiben, z.B. _\<\<1\>\>_ oder _\<\<1 von n\>\>_ (n ist die Gesamtzahl an Seiten)
1. **Text, Datum oder Nummern hinzufügen**
1. Über "Formal für Seitenzahlen und Datum kann man das **Anzeigeformat ändern** (z.B. eben 1 oder 1 von n). Hier kann man auch einstellen, dass die Seitenzahl nur auf geraden oder ungeraden Seiten angezeigt werden soll. Das ist praktisch, falls man die Seitenzahlen abwechselnd auf der linken und rechten Seite platzieren will. Dazu einfach zwei Kopf-/Fußzeilen hinzufügen mit den entsprechenden Einstellungen. Danach muss man eventuell erneut auf "Seitenzahl einfügen" klicken um die Änderungen zu übernehmen.
1. Hier wird eine **Vorschau** angezeigt
1. Wenn alles passt, kann man auf OK klicken. Man kann später auch noch jederzeit die Einstellungen anpassen, auch nach dem Schließen des Dokuments.

![Kopf- oder Fußzeile Dialog](tut/Sammel-PDFs/img/5.png)


### Globale Nummerierung bearbeiten

Sollte man die Nummerierung ändern wollen oder sollten neue Seiten hinzugefügt worden sein, blendet man - wenn nötig - die **Werkzeuge-Toolbar** wieder ein **(1)**. Anschließend klickt man auf **Kopf- und Fußzeile (2)** und auf **"Aktualisieren..." (3)**. Danach öffnet sich wieder der bekannte Dialog. Man kann, falls nur neue Seiten hinzugefügt wurden direkt auf "OK" klicken und die Nummerierung wird neu angewandt.

![Nummerierung bearbeiten](tut/Sammel-PDFs/img/6.png)