#!/bin/bash

# this script can be used to create and initialize a git repro for OMN
# it takes one argument this can ether be a filename
# or a name of a repository (located at gitlab.com/omn-dev-team/)

# if it is a filename this file must consist of repository names seperated by newlines

init_repro() {
	#check if given argument is a repro name by trying to split
	gitUrl="https://gitlab.com/omn-dev-team/$1.git"
	gitUrlConf="https://gitlab.com/omn-dev-team/conf.git"
	gitUrlCld="https://gitlab.com/omn-dev-team/$1-cld.git"
	lvaRegex="([^-]*?)-([^-]*?)-(.*)"
	LVType=$(echo $1 | sed -r "s/$lvaRegex/\1/g")
	LVNR=$(echo $1 | sed -r "s/$lvaRegex/\2/g")
	LVName=$(echo $1 | sed -r "s/$lvaRegex/\3/g")
	
	#echo "$LVNR"
	#echo "$LVName"
	#echo "$LVType"

	#basic setup
	mkdir $1
	cd $1/
	git init
	git remote add origin $gitUrl
	git submodule add $gitUrlConf

	#create cld
	mkdir cld
	cd cld
	git init
	git remote add origin $gitUrlCld
	cd ..
	git submodule add $gitUrlCld cld

	#configure repository
	mkdir -p opn/conf
	echo "$LVNR" > opn/conf/nr
	echo "$LVName" > opn/conf/name
	echo "$LVType" > opn/conf/type
	echo "\def \lvanr {$LVNR}" > opn/conf/def.tex
	echo "\def \lvaname {$LVName}" >> opn/conf/def.tex
	echo "\def \lvashort {TODO: LVASHORT}" >> opn/conf/def.tex
	echo "\def \lvatype {$LVType}" >> opn/conf/def.tex
	echo "\def \giturl {$gitUrl}" >> opn/conf/def.tex
	touch opn/conf/info.tex
	
	#do a update for final file creation
	ln -s conf/Makefile Makefile
	make update
	
	#commits erstellen
	cd ./cld
	git add --all
	git commit -m"Initial commit"
	git push origin master
	
	cd ..
	git add --all
	git commit -m"Initial commit"
	git push origin master
	cd ..
}

if [ -r "$1" ] && [ -f "$1" ]; then
	cat $1 | while read line
	do
		if [ ! -z "$line" ]; then
			init_repro $line
		fi
	done
else
	init_repro $1
fi
