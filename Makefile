input=./conf/main
output=./conf/tmp
final=./conf/out
lva-name::= $(shell cat ./opn/conf/name)
lva-nr::= $(shell cat ./opn/conf/nr)
lva-type::= $(shell cat ./opn/conf/type)

logfileName::= $(shell date +%F_%H_%M_%S).log
logfile::= $(output)/$(logfileName)

SHELL:=/bin/bash
#Zugriffsoptionen offen oder geschlossen
acess= opn cld
#Modus Prüfung (exm), Labor (lab), Skriptum 
mode= exm hlp lab skp ue
#Prüfungsmodus Beispiel (bsp), Mündlich (mdl), Multiple Choice (mpc), Theorie (thr), Text (txt)
type= bsp mdl mpc thr txt
#Sortierung nach Kapitel (chp), nach datum (date)
sorting= chp date
#Ausführung nur Angaben (ang), Lösungen (lsg), beides hybrid (hyb)
opt = ang lsg hyb
#Ausdruck optionen (cmp, so eng wie möglich oder pge, für jedes bsp eine eigene seite)
prt = cmp pge
#Seitenlayout für Einseitig oder Doppelseitig
page = one two
#Seitengeometrie maximum oder normal
geo = max avg
#Text-Style nach den Instituten
style = emce acin esea tele
#Protkolle erstellen
prot = rep bsc msc doc

#Release-Versionen
rel = ang-pge-two-max-emce lsg-cmp-two-max-emce hyb-cmp-two-max-acin hyb-pge-two-max-acin lsg-cmp-one-avg-acin ang-pge-one-avg-acin hyb-cmp-one-avg-emce hyb-pge-one-avg-emce

everything=$(foreach acess,$(acess),$(foreach mode,$(mode),$(foreach type, $(type),$(foreach opt,$(opt),$(foreach prt,$(prt),$(foreach page,$(page),$(foreach geo,$(geo),$(foreach style,$(style),$(acess)-$(mode)-$(type)-$(opt)-$(prt)-$(page)-$(geo)-$(style)))))))))

.PHONY: all list test small release $(everything) clean update file

all:	clean $(everything)
	@gzip -9 $(logfile)
	@mv $(logfile).gz $(final)/
	
# Erstellt eine liste von allen Latex dokumenten welche gefunden werden und sortiere sie nach kapiteln
list:	
	@$(foreach mode,$(mode),$(foreach type,$(type), if [ -d "./opn/$(mode)/$(type)" ] ; then find ./opn/$(mode)/$(type)/ -name *.tex| sort -t '/' -k6,6 -n -k7,7 -n -k8,8 -n| awk '{printf "\\input{%s}\n", $$1}' > $(output)/opn-$(mode)-$(type).tex; cp $(output)/opn-$(mode)-$(type).tex $(output)/cld-$(mode)-$(type).tex;fi; if [ -d "./cld/$(mode)/$(type)" ]; then find ./cld/$(mode)/$(type)/ -name *.tex| awk '{printf "\\input{%s}\n", $$1}' >> $(output)/cld-$(mode)-$(type).tex; sort -t '/' -k6,6 -n -k7,7 -n -k8,8 -n $(output)/cld-$(mode)-$(type).tex -o $(output)/cld-$(mode)-$(type).tex; fi ; if [ ! -s "$(output)/opn-$(mode)-$(type).tex" ] && [ -f "$(output)/opn-$(mode)-$(type).tex" ] ; then rm $(output)/opn-$(mode)-$(type).tex; fi; if [ ! -s "$(output)/cld-$(mode)-$(type).tex" ] && [ -f "$(output)/cld-$(mode)-$(type).tex" ] ; then rm $(output)/cld-$(mode)-$(type).tex; fi;))
# Entferne Datein aus falschen Ordnern
	@$(foreach acess, $(acess),$(foreach mode, $(mode),$(foreach type, $(type), $(foreach prot, $(prot), if [ -f "$(output)/$(acess)-$(mode)-$(type).tex" ] ; then sed -i '/$(prot)/d' $(output)/$(acess)-$(mode)-$(type).tex; sed -i '/conf/d' $(output)/$(acess)-$(mode)-$(type).tex; sed -i '/misc/d' $(output)/$(acess)-$(mode)-$(type).tex;sed -i '/tmp/d' $(output)/$(acess)-$(mode)-$(type).tex;fi; ))))
# Sortiere nach Kapiteln
	@$(foreach acess, $(acess),$(foreach mode, $(mode),$(foreach type, $(type),if [ -s "$(output)/$(acess)-$(mode)-$(type).tex" ] ; then i=0;while [ -s $(output)/$(acess)-$(mode)-$(type).tex ] && [ "$$i" -lt 30 ] ; do sed "/chp\/$${i}\//!d" $(output)/$(acess)-$(mode)-$(type).tex > $(output)/$(acess)-$(mode)-$(type)-$${i}.tex; sed -i "/chp\/$${i}\//d" $(output)/$(acess)-$(mode)-$(type).tex;if [ ! -s $(output)/$(acess)-$(mode)-$(type)-$${i}.tex ] && [ -f $(output)/$(acess)-$(mode)-$(type)-$${i}.tex ] ; then rm -f $(output)/$(acess)-$(mode)-$(type)-$${i}.tex; fi; let i=i+1 ;done; if [ ! -s $(output)/$(acess)-$(mode)-$(type).tex ] && [ -f $(output)/$(acess)-$(mode)-$(type).tex ] ; then rm -f $(output)/$(acess)-$(mode)-$(type).tex; fi; fi;)))

# Erstellt reports
rep:
	@$(foreach prot , $(prot),find ./{opn,cld}/ -name *.tex -path "*/$(prot)/*"| sed '/conf/d' | sort -t '/' -k6,6 -n -k7,7 -n -k8,8 -n| awk '{printf "\\input{%s}\n", $$1}' > $(output)/$(prot).tex;)
	
	
# Erstellt ein pdf mit den datein, welche in den letzten 60 minuten bearbeitet wurden.
test:	clean
	@find $(foreach acess,$(acess),$(foreach mode,$(mode),$(foreach type,$(type),./$(acess)/$(mode)/$(type)/))) -name *.tex -mmin -60 | sort -t '/' -k6,6 -n -k7,7 -n -k8,8 -n| awk '{printf "\\input{%s}\n", $$1}' > $(output)/test.tex;
# Entferne falsche Datein
	@sed -i '/conf/d' $(output)/test.tex; sed -i '/misc/d' $(output)/test.tex; sed -i '/tmp/d' $(output)/test.tex;
	@printf "\\input{$(input)/acess/cld.tex}" > $(output)/acess.tex;
	@printf "\\input{$(input)/mode/exm.tex}" > $(output)/mode.tex;
	@printf "\\input{$(input)/type/bsp.tex}" > $(output)/type.tex;
	@printf "\\input{$(input)/opt/hyb.tex}" > $(output)/opt.tex;
	@printf "\\input{$(input)/prt/cmp.tex}" > $(output)/prt.tex;
	@printf "\\input{$(input)/page/two.tex}" > $(output)/page.tex;
	@printf "\\input{$(input)/geo/avg.tex}" > $(output)/geo.tex;
	@printf "\\input{$(input)/style/emce.tex}" > $(output)/style.tex;
	@latexmk -pdf -synctex=1 -interaction=errorstopmode -output-directory='${output}' '${input}/test.tex';
	@mv ${output}/test.pdf $(final)

# Erstelle nur die Randbedingungen von allen Dokumenten im Errorstopmode, um Fehlermeldungen zu sehen
small:	list
	@printf "\\input{$(input)/acess/cld.tex}" > $(output)/acess.tex;
	@printf "\\input{$(input)/page/two.tex}" > $(output)/page.tex;
	@printf "\\input{$(input)/geo/avg.tex}" > $(output)/geo.tex;
	@printf "\\input{$(input)/style/emce.tex}" > $(output)/style.tex;
	@$(foreach mode,$(mode),$(foreach type,$(type), if [ -f "./opn/conf/$(mode)-$(type).tex" ] ; then printf "\\input{$(input)/input.tex}" > $(output)/input.tex; printf "Generating cld-$(mode)-$(type)-hyb-cmp-two-avg-emce .... "; printf "\\input{$(input)/mode/$(mode).tex}" > $(output)/mode.tex; printf "\\input{$(input)/type/$(type).tex}" > $(output)/type.tex; printf "\\input{$(input)/opt/hyb.tex}" > $(output)/opt.tex; printf "\\input{$(input)/prt/cmp.tex}" > $(output)/prt.tex; latexmk -f -pdf -synctex=1 -interaction=errorstopmode -output-directory='${output}' '${input}/main.tex'; mkdir -p $(final)/$(mode)/$(type); mv ${output}/main.pdf $(final)/$(mode)/$(type)/$(lva-type)-$(lva-nr)-$(lva-name)-cld-$(mode)-$(type)-hyb-cmp-two-avg-emce.pdf; printf "Done\n"; fi;))
	
# Erstelle alle open Builds mit den häufigsten configurationen
release:	list
	@$(foreach mode,$(mode),$(foreach type,$(type),$(foreach rel,$(rel), if [ -f "./opn/conf/$(mode)-$(type).tex" ] ; then make opn-$(mode)-$(type)-$(rel);fi;)))
	
# Definiert alle möglichen Build optionen
define PROGRAMM_temp =
$(1)-$(2)-$(3)-$(4)-$(5)-$(6)-$(7)-$(8): list
	@if [ -f "./opn/conf/$(mode)-$(type).tex" ]; then printf "\\input{$(input)/input.tex}" > $(output)/input.tex; printf "Generating $(1)-$(2)-$(3)-$(4)-$(5)-$(6)-$(7)-$(8) .... ";  if [ -f "$(input)/acess/$(1).tex" ]; then printf "\\input{$(input)/acess/$(1).tex}" > $(output)/acess.tex ; if [ -f "$(input)/mode/$(2).tex" ]; then printf "\\input{$(input)/mode/$(2).tex}" > $(output)/mode.tex ; if [ -f "$(input)/type/$(3).tex" ]; then printf "\\input{$(input)/type/$(3).tex}" > $(output)/type.tex ; if [ -f "$(input)/opt/$(4).tex" ]; then printf "\\input{$(input)/opt/$(4).tex}" > $(output)/opt.tex ; if [ -f "$(input)/prt/$(5).tex" ]; then printf "\\input{$(input)/prt/$(5).tex}" > $(output)/prt.tex ; if [ -f "$(input)/page/$(6).tex" ]; then printf "\\input{$(input)/page/$(6).tex}" > $(output)/page.tex ; if [ -f "$(input)/geo/$(7).tex" ]; then printf "\\input{$(input)/geo/$(7).tex}" > $(output)/geo.tex ; if [ -f "$(input)/style/$(8).tex" ]; then printf "\\input{$(input)/style/$(8).tex}" > $(output)/style.tex ;latexmk -f -pdf -synctex=1 -interaction=nonstopmode -output-directory='${output}' '${input}/main.tex' &>>$(logfile); mkdir -p $(final)/$(2)/$(3); mv ${output}/main.pdf $(final)/$(2)/$(3)/$(lva-type)-$(lva-nr)-$(lva-name)-$(1)-$(2)-$(3)-$(4)-$(5)-$(6)-$(7)-$(8).pdf; printf "Done\n";  else printf "\nStyle describing file not found at ./conf/main/style/\n";fi; else printf "\nGeometry describing file not found at ./conf/main/geo/\n" ;fi; else printf "\nPage describing file not found at ./conf/main/page/\n" ; fi; else printf "\nPrinting describing file not found at ./conf/main/prt/\n"; fi; else printf "\nOption describing file not found at ./conf/main/opt/\n"; fi; else printf "\nType describing file not found at ./conf/main/type/\n" ; fi; else printf "\nMode describing file not found at ./conf/main/type/\n" ; fi ; else printf "\nAcess describing file not found at ./conf/main/acess/\n" ;fi ; fi;

endef

#define PROGRAMM_acess =
#$(1):	list
	#@$(foreach mode,$(mode),$(foreach type, $(type),$(foreach opt,$(opt),$(foreach prt,$(prt),$(foreach page,$(page),$(foreach geo,$(geo),$(foreach style,$(style),

$(foreach acess,$(acess),$(foreach mode,$(mode),$(foreach type, $(type),$(foreach opt,$(opt),$(foreach prt,$(prt),$(foreach page,$(page),$(foreach geo,$(geo),$(foreach style,$(style),$(eval $(call PROGRAMM_temp,$(acess),$(mode),$(type),$(opt),$(prt),$(page),$(geo),$(style)))))))))))

#$(foreach acess,$(acess),$(eval $(call PROGRAMM_acess,$(acess))))

# Sauber machen
clean:
	@find opn/ -mindepth 1 -type d -empty -delete
	@find cld/ -mindepth 1 -type d -empty -delete
	@rm -f $(output)/*.{tex,ps,pdf,fls,cpt,fdb,log,aux,out,dvi,bbl,blg,toc,synctex.gz}*
	@rm -rf $(final)/*
	
# Neue README, TODO, und LICENSE files in die repos kopieren
update:
	@shopt -s dotglob; for file in ./*; do if [ -f "$$file" ]  && [[ "$$file" != *.git ]] && [[ "$$file" != *Makefile ]] && [[ "$$file" != *.gitmodules ]]; then rm $$file; fi; done;
	@shopt -s dotglob; cp ./conf/init/opn/* ./
	@shopt -s dotglob; for file in ./cld/*; do if [ -f "$$file" ]  && [[ "$$file" != *.git ]] && [[ "$$file" != *Makefile ]] && [[ "$$file" != *.gitmodules ]]; then rm $$file; fi; done;
	@shopt -s dotglob; if [ -d "./cld" ]; then cp ./conf/init/cld/* ./cld/; fi;

# Leere Dateistruktur erstellen um einfach neue inhalte hinzufügen zu können
file:
	@mkdir -p {opn,cld}/{exm,hlp,lab,skp,ue}/{bsp,mdl,mpc,thr,txt}/{chp,date,misc}
